<?php
if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// T
	'soapsympa_description' => 'Ce plugin est un web service relié au système de listes SYMPA. 
Il permet : 
-# De gérer les abonnements aux listes depuis l\'espace privé de SPIP. 
-# Pour chaque auteur SPIP, de voir et gérer ses abonnements aux listes. 
-# De proposer un formulaire d\'inscription aux listes depuis le site public. 

Les services utilisés sont décrits ici http://www.sympa.org/manual/soap.',
	'soapsympa_slogan' => 'Savonner SPIP pour accéder à SYMPA',


);

?>
